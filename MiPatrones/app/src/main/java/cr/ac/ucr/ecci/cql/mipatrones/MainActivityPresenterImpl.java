package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Context;
import android.os.Bundle;
import android.widget.ListView;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;
import java.util.List;

public class MainActivityPresenterImpl extends FragmentActivity implements
        // Implementacion de MainActivityPresenter de la Capa (P)
        MainActivityPresenter,
        // La interface OnFinishedListener de GetListItemsInteractor para obtener el valor deretorno de la capa Iteractor (P)
        GetListItemsInteractor.OnFinishedListener {

    private MainActivityView mMainActivityView;
    private GetListItemsInteractor mGetListItemsInteractor;
    private  Context context;
    private ListView mListView;

    public MainActivityPresenterImpl(MainActivityView mainActivityView, Context context) {
        this.mMainActivityView = mainActivityView;
        this.context = context;
        // Capa de negocios (Interactor)
        this.mGetListItemsInteractor = new GetListItemsInteractorImpl();

    }

    @Override
    public void onResume() {
        if (mMainActivityView != null) {
            mMainActivityView.showProgress();
        }

        // Obtener los items de la capa de negocios (Interactor)
        mGetListItemsInteractor.getItems(this,context);
    }

    // Evento de clic en la lista
    @Override
    public void onItemClicked(int position, List<Persona> list) {
        if (mMainActivityView != null) {
            mMainActivityView.showMessage(String.format("Position %d clicked", position + 1));

        }
    }

    @Override
    public void onDestroy() {
        mMainActivityView = null;
    }

    @Override
    public void onFinished(List<Persona> items) {
        if (mMainActivityView != null) {

            mMainActivityView.setItems(items);

            mMainActivityView.hideProgress();
        }
    }

    // Retornar la vista
    public MainActivityView getMainView() {
        return mMainActivityView;
    }
}
