package cr.ac.ucr.ecci.cql.mipatrones;

import android.provider.BaseColumns;

public class DataBaseContract {
    public static class DataBaseEntry implements BaseColumns{
        public static final String TABLE_NAME_PERSON= "Persona";

        public static final String COLUMN_NAME_identificacion = "identificacion";
        public static final String COLUMN_NAME_nombre= "nombre";
        public static final String COLUMN_NAME_idImagen = "idImagen";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String INTEGER_TYPE = " INTEGER";


    public static final String SQL_CREATE_PERSON = "CREATE TABLE " + DataBaseEntry.TABLE_NAME_PERSON +"("
            + DataBaseEntry._ID + TEXT_TYPE + " PRIMARY KEY," +
            DataBaseEntry.COLUMN_NAME_nombre + TEXT_TYPE + COMMA_SEP +
            DataBaseEntry.COLUMN_NAME_idImagen + INTEGER_TYPE  + ")";

    public static final String SQL_DELETE_PERSON =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_PERSON;
}
