package cr.ac.ucr.ecci.cql.mipatrones;
import java.util.Arrays;
import java.util.List;
import java.lang.Object;
import android.content.Context;
public class DataBaseDataSourceImpl implements DataBaseDataSource {
    Persona persona;
    DataBaseHelper dataBaseHelper;

    public DataBaseDataSourceImpl(){
        persona = new Persona();
    }

    @Override
    public List<Persona> obtainItems(Context context) throws BaseDataItemsException {

        List<Persona> items = null;

        try {

            // TODO: Obtener de la base de datos
            items = createArrayList(context);

        } catch (Exception e) {
            throw new BaseDataItemsException(e.getMessage());
        }

        return items;
    }
    private List<Persona> createArrayList(Context context) {
        persona.leer(context);
        return  persona.fileName;
    }
}
