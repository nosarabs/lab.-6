package cr.ac.ucr.ecci.cql.mipatrones;

import java.util.ArrayList;
import java.util.List;

public interface MainActivityPresenter {

    // resumir
    void onResume();

    // evento cuando se hace clic en la lista de elementos
    void onItemClicked(int position, List<Persona> p);

    // destruir
    void onDestroy();
}
