package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Context;

import java.util.List;

public interface ItemsRepository {

    List<Persona> obtainItems(Context context) throws CantRetrieveItemsException;
}
