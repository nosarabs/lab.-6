package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Persona implements Parcelable {

    List<Persona> fileName = new ArrayList<>();

    private String identificacion;
    private String nombre;
    private int idImagen;

    public Persona(String identificacion,String nombre,int idImagen){
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.idImagen = idImagen;

    }

    public Persona(){}

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setIdImagen(int idImagen) {
        this.idImagen = idImagen;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public int getIdImagen() {
        return idImagen;
    }

    public void leer (Context context){
        // usar la clase DataBaseHelper para realizar la operacion de leer
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        // Obtiene la base de datos en modo lectura
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        // Define cuales columnas quiere solicitar // en este caso todas las de la clase
        String[] projection = {
                DataBaseContract.DataBaseEntry._ID,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_nombre,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_idImagen,


        };
        // Filtro para el WHERE
        String selection = DataBaseContract.DataBaseEntry._ID + " = ?";
        String[] selectionArgs = {};
        // Resultados en el cursor
        Cursor cursor = db.query(
                DataBaseContract.DataBaseEntry.TABLE_NAME_PERSON, // tabla
                projection, // columnas
                null, // where
                null, // valores delwhere
                null, // agrupamiento
                null, // filtros po grupo
                null // orden
        );
        // recorrer los resultados y asignarlos a la clase // aca podriaimplementarse un ciclo si es necesario
        if(cursor.moveToFirst() && cursor.getCount() > 0) {
            while (!cursor.isAfterLast()) {
                Persona x = new Persona();
                x.setNombre(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry._ID)));
                x.setIdentificacion(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_nombre)));
                x.setIdImagen(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_idImagen));

                fileName.add(x);
                cursor.moveToNext();
            }

        }
    }
    @Override
    public  String toString(){

        return this.getNombre();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
