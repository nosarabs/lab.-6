package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Context;

import java.util.List;

public class ItemsRepositoryImpl implements ItemsRepository {

    private DataBaseDataSource mDataBaseDataSource;

    @Override
    public List<Persona> obtainItems(Context context) throws CantRetrieveItemsException {

        List<Persona> items = null;
        // Se realizan las llamadas a las fuentes de datos de donde se obtienen los datos
        //    Ejemplo: base de datos local, archivos locales, archivos en la red, bases de datos en la red

        try {

            mDataBaseDataSource = new DataBaseDataSourceImpl();
            items = mDataBaseDataSource.obtainItems(context);

        } catch (BaseDataItemsException e) {
            throw new CantRetrieveItemsException(e.getMessage());
        }

        return items;
    }
}
