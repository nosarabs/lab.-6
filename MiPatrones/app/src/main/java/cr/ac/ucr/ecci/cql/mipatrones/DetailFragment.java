package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class DetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    Persona persona;
    public DetailFragment() {
        // Required empty public constructor
    }


    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        TextView personDetail = (TextView)view.findViewById(R.id.detail);

        // Se 'desencapsula' el objeto seleccionado recibido desde la activida principal
        persona = (Persona) getArguments().getParcelable("selected_data");

        // Revisa si este no está vacío
        if (persona != null) {

            // Asigna al componente del layout el valor correspondiete de descripción completa
            personDetail.setText(persona.getIdentificacion());
        }

        return view;

    }
}
