package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

public class DataBaseHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "AndroidStorage.db";



    // Nombre de la base de datos
    // constructor de la clase, el contexto tiene la informacion global sobre el ambiente de la app
    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // implementamos el metodo para la creacion de la base de datos
    public void onCreate(SQLiteDatabase db) {
        // Crear la base de datos de la app
        db.execSQL(DataBaseContract.SQL_CREATE_PERSON);
        populate(db);
    }
    // implementamos el metodo para la actualizacion de la base de datos
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Administracion de actualizaciones
        db.execSQL(DataBaseContract.SQL_DELETE_PERSON);
        db.execSQL(DataBaseContract.SQL_CREATE_PERSON);
        onCreate(db);
    }
    // inplementamos el metodo para volver a la version anterior de la base de datos
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void populate(SQLiteDatabase db){
        db.execSQL("INSERT INTO Persona VALUES ('Aaron','1',1)");
        db.execSQL("INSERT INTO Persona VALUES ('Berta','2',2)");
        db.execSQL("INSERT INTO Persona VALUES ('Denisse','3',3)");
        db.execSQL("INSERT INTO Persona VALUES ('Andres','4',4)");
        db.execSQL("INSERT INTO Persona VALUES ('Diana','5',5)");
    }

}
