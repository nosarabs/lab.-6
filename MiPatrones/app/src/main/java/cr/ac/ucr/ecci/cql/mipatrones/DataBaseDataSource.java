package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Context;

import java.util.List;

public interface DataBaseDataSource {

    List<Persona> obtainItems (Context context) throws BaseDataItemsException;
}

